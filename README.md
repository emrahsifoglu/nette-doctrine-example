# Nette + Doctrine example

This is a simple application in Nette Framework with Doctrine implementation.

## Table of Contents

* [Getting Started](#getting-started)
  * [Prerequisite](#prerequisite)
  * [Installation](#installation)
* [Running](#running)
* [Built with](#built-with)
  * [Development Environment](#development-environment)
* [Authors](#authors)
* [License](#license)
* [Resources](#resources)

## Getting Started

These instructions will guide you to up and running the project on your local machine.

### Prerequisite

* Composer 
* Apache( or Nginx)
* MySQL

### Installation

First you need to create `config.local.neon` in app/config then define params to connect MySQL.

```neon
doctrine:
    user:
    password:
    dbname: nette-article
    host: localhost
```

Now you can run `composer install` to install dependencies.

Finally you can create table(s) on database with following command `php www/index.php migration:migrate`.

## Running

You can start the server with `php -S localhost:8000 -t www`.

| Method | Path                         | Info         |
| ------ | ---------------------------  | -------------|
| GET | [/](http://localhost:8000/) | list of articles |

There is only one route where you can see all articles. As default a new article will be created each time you refresh the page.

## Testing

There are no actual test but just one to only see whether configuration works properly.

## Built with

#### Development Environment

* [Ubuntu](https://www.ubuntu.com/download/server) - Server
* [MySQL](https://www.mysql.com/) - Database
* [PHP](http://php.net/) - Language
* [Linux Mint 18](https://www.linuxmint.com/) - OS
* [PhpStorm](https://www.jetbrains.com/phpstorm/) - IDE
* [Chromium](https://www.chromium.org/Home) - Browser

## Authors

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](- https://github.com/emrahsifoglu)

## License

This project is licensed under the [MIT License](http://opensource.org/licenses/MIT).

## Resources

- http://www.petrfidler.cz/blog/spojeni-nette-a-doctrine
- https://github.com/Maxell92/example-nette-doctrine
- https://doc.nette.org/en/2.4/quickstart/home-page
- https://github.com/DeprecatedPackages/DoctrineMigrations
- https://forum.nette.org/cs/15393-nette-v-konzoli-aneb-integrace-symfony-console-kdyby-console
- https://stackoverflow.com/questions/9224754/how-to-remove-origin-from-git-repository
