<?php

require __DIR__ . '/../../app/Greeting.php';

use App\Greeting;
use PHPUnit\Framework\TestCase;

final class GreetingTest extends TestCase
{

    public function testSame() {
        $o = new Greeting();
        $this->assertEquals('Hello John', $o->say('John'));
    }

}
