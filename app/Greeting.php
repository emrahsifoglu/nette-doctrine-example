<?php

namespace App;

use Nette\InvalidArgumentException;

class Greeting
{

    /**
     * @param $name
     * @return string
     */
    public function say($name) {
        if (!$name) {
            throw new InvalidArgumentException('Invalid name');
        }
        return "Hello $name";
    }

}
