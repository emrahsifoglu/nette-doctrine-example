<?php

namespace App\Presenters;

use App\Service\ArticleService;
use Nette;

class HomepagePresenter extends Nette\Application\UI\Presenter
{

    /** @var ArticleService @inject */
    public $articleService;

    /**
     * @throws \Exception
     */
    public function actionDefault() {
        $this->articleService->createArticle();
    }

    public function renderDefault() {
        $this->template->articles = $this->articleService->getArticles();
    }

}
