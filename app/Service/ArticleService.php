<?php

namespace App\Service;

use App\Entity\Article\Article;
use Kdyby\Doctrine\EntityManager;

class ArticleService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ArticleService constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Exception
     */
    public function createArticle() {
        $article = new Article();
        $article->translate('cs')->setName('Název článku');
        $article->translate('en')->setName('Name of article');
        $article->mergeNewTranslations();

        $this->entityManager->persist($article);
        $this->entityManager->flush();
    }

    /**
     * @return array
     */
    public function getArticles() {
        return $this->entityManager->getRepository(Article::class)->findAll();
    }

}
